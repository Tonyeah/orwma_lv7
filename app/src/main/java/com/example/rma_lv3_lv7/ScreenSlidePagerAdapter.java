package com.example.rma_lv3_lv7;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {

    private static  final int NUM_PAGES = 4;
    Context parentContext;

    public ScreenSlidePagerAdapter(@NonNull FragmentManager fm, Context parentContext){
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.parentContext = parentContext;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return InputFragment.newInstance();
            case 1:
                return MessageFragment.newInstance(parentContext);
            case 2:
                return ModularFragment.newInstance(EViewType.TEXT);
            case 3:
                return ModularFragment.newInstance(EViewType.IMAGE);
        }
        return SlideFragment.newInstance("Given position argument is out of bounds");
    }

    @Override
    public int getCount() {
        return NUM_PAGES;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return "Tab #" + (position+1);
    }
}
