package com.example.rma_lv3_lv7;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

public class MessageFragment extends Fragment {

    private static final String MESSAGE_KEY = "display_message";
    private TextView mMessageTv;
    private String message = "...";
    private MainActivity mainActivity;

    public void setMessage(String msg){
       message=msg;
    }

    public static MessageFragment newInstance(Context mainActivityContextReference) {
        MessageFragment fragment = new MessageFragment();
        fragment.mainActivity = (MainActivity) mainActivityContextReference;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_message, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mMessageTv = (TextView) view.findViewById(R.id.tvMessage);
    }

    @Override
    public void onResume() {
        super.onResume();
        displayMessage(mainActivity.getMessage());
    }

    private void displayMessage(String message) {
        mMessageTv.setText(!message.trim().isEmpty() ? message : "...");
    }
}