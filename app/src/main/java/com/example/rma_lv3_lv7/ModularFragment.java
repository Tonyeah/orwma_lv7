package com.example.rma_lv3_lv7;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;

public class ModularFragment extends Fragment {

    private EViewType viewType = EViewType.TEXT;

    private void setViewType(EViewType viewType){
        this.viewType = viewType;
    }

    public static ModularFragment newInstance(EViewType viewType) {
        ModularFragment fragment = new ModularFragment();
        fragment.setViewType(viewType);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_modular, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setUpViews(view);
    }

    @SuppressLint("ResourceAsColor")
    private void setUpViews(View view) {
        switch (viewType){
            case TEXT:
                TextView textView = new TextView(getContext());
                textView.setId(ViewCompat.generateViewId());
                textView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT));
                textView.setText("Default_modular_fragment_text");
                textView.setTextColor(Color.parseColor("#222222"));
                textView.setTextSize(25);
                textView.setGravity(Gravity.CENTER);
                textView.setBackgroundColor(Color.parseColor("#cccccc"));
                ((LinearLayout)view).addView(textView, 0);
                break;
            case IMAGE:
                ImageView imageView = new ImageView(getContext());
                imageView.setId(ViewCompat.generateViewId());
                imageView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT));
                imageView.setImageResource(R.drawable.lava_dragon);
                imageView.setBackgroundColor(Color.parseColor("#cccccc"));
                ((LinearLayout)view).addView(imageView, 0);
                break;
            default:
                Toast.makeText(getContext(), "No view type specified!", Toast.LENGTH_LONG).show();
        }
    }
}
