package com.example.rma_lv3_lv7;

public interface ButtonClickListener {
    void onButtonClicked(String message);
}
