package com.example.rma_lv3_lv7;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;

import org.w3c.dom.Text;

import java.util.List;

public class MainActivity extends AppCompatActivity implements ButtonClickListener{

    private ViewPager mViewPager;
    private TabLayout mTabLayout;
    private String message = "...";

    public String getMessage(){
        return message;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        setUpPager();
    }

    private void initViews() {
        mViewPager = findViewById(R.id.viewPager);
        mTabLayout = findViewById(R.id.tab);
        mTabLayout.setupWithViewPager(mViewPager);
        mViewPager.setPageTransformer(false, new ViewPager.PageTransformer(){
            public void transformPage(View page, float position) {
                page.setRotationY(position * -45);
            }
        });
    }

    private void setUpPager() {
        PagerAdapter adapter = new ScreenSlidePagerAdapter(getSupportFragmentManager(), this);
        mViewPager.setAdapter(adapter);
    }

    @Override
    public void onButtonClicked(String message) {
        this.message = message;
//        if(((MessageFragment)((ScreenSlidePagerAdapter) mViewPager.getAdapter()).getItem(1)) == null){
//            Toast.makeText(this, "messagefragment is null, oh no!", 4).show();
//        }
//        else if((((ScreenSlidePagerAdapter) mViewPager.getAdapter()).getItem(1)) instanceof MessageFragment){
//            ((MessageFragment) ((ScreenSlidePagerAdapter) mViewPager.getAdapter()).getItem(1)).setMessage(message);
//        }
    }
}